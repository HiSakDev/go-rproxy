FROM debian:buster-slim
LABEL maintainer="HiSakDev <sak.devac@gmail.com>"

RUN apt-get update \
    && apt-get install -y ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ADD grproxy /app/grproxy
WORKDIR /app
USER 1000
EXPOSE 8080
ENTRYPOINT ["/app/grproxy"]