package main

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/BurntSushi/toml"
)

type duration struct {
	time.Duration
}

func (d duration) MarshalText() ([]byte, error) {
	return []byte(d.Duration.String()), nil
}

func (d *duration) UnmarshalText(text []byte) (err error) {
	d.Duration, err = time.ParseDuration(string(text))
	return
}

type customError struct {
	File           string
	ContentType    string
	StatusCode     int
	ResponseHeader []modifyField
	ResponseCookie []*http.Cookie
}

type customTLS struct {
	Cert                string
	Key                 string
	MinVersion          uint16
	MaxVersion          uint16
	Ciphers             []uint16
	PreferServerCiphers bool
	Http2               bool
	ClientCA            string
	ClientAuth          tls.ClientAuthType
}

type reverseProxyConfig struct {
	Target          []reverseProxyTarget
	Plugin          reverseProxyPlugin
	Error           customError
	TLS             customTLS
	Listen          string
	ShutdownTimeout duration
}

var config reverseProxyConfig

func (c *reverseProxyConfig) Decode(path string) error {
	if _, err := toml.DecodeFile(path, c); err != nil {
		return err
	}
	return nil
}
