package certreload

import (
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type CAPool struct {
	mutex         sync.RWMutex
	stopMutex     sync.RWMutex
	done          chan bool
	caFile        string
	caStat        os.FileInfo
	CAs           *x509.CertPool
	Logger        *log.Logger
	WatchInterval time.Duration
	NotifyCh      chan bool
	RawData       [][]byte
}

func NewPool(caFile string) (p CAPool, err error) {
	if p.caFile, err = filepath.Abs(caFile); err == nil {
		p.CAs = x509.NewCertPool()
		if err = p.load(); err == nil {
			p.WatchInterval = 1 * time.Minute
		}
	}
	return
}

func NewPoolWithSystem(caFile string) (p CAPool, err error) {
	if p.caFile, err = filepath.Abs(caFile); err == nil {
		if p.CAs, err = x509.SystemCertPool(); err == nil {
			if err = p.load(); err == nil {
				p.WatchInterval = 1 * time.Minute
			}
		}
	}
	return
}

func (p *CAPool) log(format string, v ...interface{}) {
	if p.Logger != nil {
		p.Logger.Printf("capool: "+format, v...)
	}
}

func (p *CAPool) load() (err error) {
	var caCert []byte
	if p.caStat, err = os.Stat(p.caFile); err == nil {
		p.mutex.Lock()
		defer p.mutex.Unlock()
		if caCert, err = ioutil.ReadFile(p.caFile); err == nil {
			for len(caCert) > 0 {
				var block *pem.Block
				block, caCert = pem.Decode(caCert)
				if block == nil {
					break
				} else if contains(p.RawData, block.Bytes) {
					continue
				}
				if cert, err := x509.ParseCertificate(block.Bytes); err != nil {
					return err
				} else {
					p.CAs.AddCert(cert)
					p.RawData = append(p.RawData, block.Bytes)
				}
			}
		}
	}
	return
}

func (p *CAPool) notify(v bool) {
	if p.NotifyCh != nil {
		p.NotifyCh <- v
	}
}

func (p *CAPool) Watch() {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	if p.done == nil {
		p.done = make(chan bool)
		go func() {
			p.log("begin watching %s", p.caFile)
			defer p.log("end of watch %s", p.caFile)
			t := time.NewTicker(p.WatchInterval)
			for {
				select {
				case <-t.C:
					if caStat, err := os.Stat(p.caFile); err == nil {
						if p.caStat.ModTime() != caStat.ModTime() || p.caStat.Size() != caStat.Size() {
							if err := p.load(); err != nil {
								p.log("can't load cert file: %v", err)
								p.notify(false)
							} else {
								p.log("reload success (add CA)")
								p.notify(true)
							}
						}
					}
				case <-p.done:
					t.Stop()
					close(p.done)
					return
				}
			}
		}()
	}
}

func (p *CAPool) Stop() {
	p.stopMutex.Lock()
	defer p.stopMutex.Unlock()
	if p.done != nil {
		p.done <- true
		<-p.done
		p.done = nil
	}
}
