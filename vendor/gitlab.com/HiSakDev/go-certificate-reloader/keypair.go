package certreload

import (
	"crypto/tls"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type KeyPair struct {
	mutex         sync.RWMutex
	stopMutex     sync.RWMutex
	done          chan bool
	certFile      string
	certStat      os.FileInfo
	keyFile       string
	keyStat       os.FileInfo
	Certificate   *tls.Certificate
	Logger        *log.Logger
	WatchInterval time.Duration
	NotifyCh      chan bool
}

func New(certFile string, keyFile string) (k KeyPair, err error) {
	if k.certFile, err = filepath.Abs(certFile); err == nil {
		if k.keyFile, err = filepath.Abs(keyFile); err == nil {
			if err = k.load(); err == nil {
				k.WatchInterval = 1 * time.Minute
			}
		}
	}
	return
}

func (k *KeyPair) log(format string, v ...interface{}) {
	if k.Logger != nil {
		k.Logger.Printf("keypair: "+format, v...)
	}
}

func (k *KeyPair) load() (err error) {
	var certificate tls.Certificate
	if k.certStat, err = os.Stat(k.certFile); err == nil {
		if k.keyStat, err = os.Stat(k.keyFile); err == nil {
			k.mutex.Lock()
			defer k.mutex.Unlock()
			if certificate, err = tls.LoadX509KeyPair(k.certFile, k.keyFile); err == nil {
				k.Certificate = &certificate
			}
		}
	}
	return
}

func (k *KeyPair) notify(v bool) {
	if k.NotifyCh != nil {
		k.NotifyCh <- v
	}
}

func (k *KeyPair) Watch() {
	k.mutex.Lock()
	defer k.mutex.Unlock()
	if k.done == nil {
		k.done = make(chan bool)
		go func() {
			k.log("begin watching %s %s", k.certFile, k.keyFile)
			defer k.log("end of watch %s %s", k.certFile, k.keyFile)
			t := time.NewTicker(k.WatchInterval)
			for {
				select {
				case <-t.C:
					if certStat, err := os.Stat(k.certFile); err == nil {
						if keyStat, err := os.Stat(k.keyFile); err == nil {
							if k.certStat.ModTime() != certStat.ModTime() || k.certStat.Size() != certStat.Size() ||
								k.keyStat.ModTime() != keyStat.ModTime() || k.keyStat.Size() != keyStat.Size() {
								if err := k.load(); err != nil {
									k.log("can't load cert or key file: %v", err)
									k.notify(false)
								} else {
									k.log("reload success")
									k.notify(true)
								}
							}
						}
					}
				case <-k.done:
					t.Stop()
					close(k.done)
					return
				}
			}
		}()
	}
}

func (k *KeyPair) Stop() {
	k.stopMutex.Lock()
	defer k.stopMutex.Unlock()
	if k.done != nil {
		k.done <- true
		<-k.done
		k.done = nil
	}
}

func (k *KeyPair) GetCertificate(*tls.ClientHelloInfo) (*tls.Certificate, error) {
	k.mutex.RLock()
	defer k.mutex.RUnlock()
	return k.Certificate, nil
}

func (k *KeyPair) GetClientCertificate(*tls.CertificateRequestInfo) (*tls.Certificate, error) {
	k.mutex.RLock()
	defer k.mutex.RUnlock()
	return k.Certificate, nil
}
