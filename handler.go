package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"time"
)

type customResponse struct {
	http.ResponseWriter
	http.Flusher
	http.Hijacker
	statusCode    int
	contentLength int64
}

func (rw *customResponse) WriteHeader(statusCode int) {
	rw.statusCode = statusCode
	rw.ResponseWriter.WriteHeader(statusCode)
}

func (rw *customResponse) Write(data []byte) (n int, err error) {
	n, err = rw.ResponseWriter.Write(data)
	rw.contentLength += int64(n)
	return
}

func accessLogHandler(h http.Handler) http.Handler {
	accessLog := log.New(os.Stdout, "", log.LstdFlags)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		addr, method, uri, proto, start := r.RemoteAddr, r.Method, r.RequestURI, r.Proto, time.Now()
		user := "-"
		if u, _, ok := r.BasicAuth(); ok {
			user = u
		}
		customRes := &customResponse{ResponseWriter: w}
		if flusher, ok := w.(http.Flusher); ok {
			customRes.Flusher = flusher
		}
		if hijacker, ok := w.(http.Hijacker); ok {
			customRes.Hijacker = hijacker
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, "ctx", &ctx)
		defer func() {
			if host, _, err := net.SplitHostPort(addr); err == nil {
				addr = host
			}
			backend := ""
			if forward, ok := ctx.Value("forward").(string); ok {
				backend = " forwarding " + forward
			}
			if res, ok := ctx.Value("response").(*http.Response); ok {
				customRes.statusCode = res.StatusCode
				if res.ContentLength >= 0 {
					customRes.contentLength = res.ContentLength
				}
			}
			accessLog.Printf("%s %s %s %s =>%s generated %d bytes in %v msecs (%s %d)",
				addr, user, method, uri, backend, customRes.contentLength,
				time.Now().Sub(start).Milliseconds(), proto, customRes.statusCode)
		}()
		h.ServeHTTP(customRes, r.WithContext(ctx))
	})
}
