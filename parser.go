package main

import (
	"crypto/tls"
	"flag"
	"net/url"
	"time"

	"github.com/kelseyhightower/envconfig"
)

type environConfig struct {
	TargetURL              *customURL         `split_words:"true"`
	ListenAddr             string             `split_words:"true" default:":8080"`
	ConfigFile             string             `split_words:"true"`
	PluginFile             string             `split_words:"true"`
	PluginConfig           string             `split_words:"true"`
	TlsCert                string             `split_words:"true"`
	TlsKey                 string             `split_words:"true"`
	TlsMinVersion          uint16             `split_words:"true"`
	TlsMaxVersion          uint16             `split_words:"true"`
	TlsCiphers             []uint16           `split_words:"true"`
	TlsPreferServerCiphers bool               `split_words:"true"`
	TlsHttp2               bool               `split_words:"true" default:"true"`
	TlsClientCa            string             `split_words:"true"`
	TlsClientAuth          tls.ClientAuthType `split_words:"true"`
	ShutdownTimeout        time.Duration      `split_words:"true" default:"30s"`
}

var env environConfig

func settingParse() error {
	if err := envconfig.Process("GRPROXY", &env); err != nil {
		return err
	}
	if env.TargetURL.URL != nil {
		config.Target = []reverseProxyTarget{{URL: env.TargetURL}}
	}
	config.Listen = env.ListenAddr
	config.Plugin.File = env.PluginFile
	config.Plugin.Config = env.PluginConfig
	config.TLS.Cert = env.TlsCert
	config.TLS.Key = env.TlsKey
	config.TLS.MinVersion = env.TlsMinVersion
	config.TLS.MaxVersion = env.TlsMaxVersion
	config.TLS.Ciphers = env.TlsCiphers
	config.TLS.PreferServerCiphers = env.TlsPreferServerCiphers
	config.TLS.Http2 = env.TlsHttp2
	config.TLS.ClientCA = env.TlsClientCa
	config.TLS.ClientAuth = env.TlsClientAuth
	config.ShutdownTimeout.Duration = env.ShutdownTimeout

	targetUrl := flag.String("target", "", "proxy target url")
	listenAddr := flag.String("listen", "", "proxy listen address")
	configFile := flag.String("config", "", "proxy config")
	pluginFile := flag.String("plugin", "", "proxy plugin")
	tlsCert := flag.String("cert", "", "proxy tls cert")
	tlsKey := flag.String("key", "", "proxy tls key")
	disableHttp2 := flag.Bool("disable-http2", false, "proxy disable HTTP/2")
	shutdownTimeout := flag.String("shutdown-timeout", "", "proxy shutdown timeout duration")
	flag.Parse()
	if *targetUrl != "" {
		if u, err := url.Parse(*targetUrl); err != nil {
			return err
		} else {
			config.Target = []reverseProxyTarget{{URL: &customURL{URL: u}}}
		}
	}
	if *listenAddr != "" {
		config.Listen = *listenAddr
	}
	if *configFile != "" {
		env.ConfigFile = *configFile
	}
	if *pluginFile != "" {
		config.Plugin.File = *pluginFile
	}
	if *tlsCert != "" {
		config.TLS.Cert = *tlsCert
	}
	if *tlsKey != "" {
		config.TLS.Key = *tlsKey
	}
	if *disableHttp2 {
		config.TLS.Http2 = false
	}
	if *shutdownTimeout != "" {
		if d, err := time.ParseDuration(*shutdownTimeout); err != nil {
			return err
		} else {
			config.ShutdownTimeout.Duration = d
		}
	}
	return nil
}

func configLoad() error {
	if env.ConfigFile != "" {
		return config.Decode(env.ConfigFile)
	}
	return nil
}
