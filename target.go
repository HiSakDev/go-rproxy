package main

import (
	"net/http"
	"net/url"
	"strings"
)

type customURL struct {
	*url.URL
}

func (u *customURL) UnmarshalText(text []byte) (err error) {
	u.URL, err = url.Parse(string(text))
	return
}

type modifyField struct {
	Name  string
	Value string
}

type matchField struct {
	Name            string
	Value           string
	Contains        bool
	CaseInsensitive bool
}

type reverseProxyTarget struct {
	URL            *customURL
	Name           string
	Domain         string
	Path           string
	Rewrite        string
	RequestHeader  []modifyField
	ResponseHeader []modifyField
	Header         []matchField
	Query          []matchField
	Cookie         []matchField
	Error          *customError
}

func (t *reverseProxyTarget) RewritePath(path string) string {
	if t.Rewrite == "" {
		return path
	}
	return strings.Replace(path, t.Path, t.Rewrite, 1)
}

func (t *reverseProxyTarget) Match(domain string, req *http.Request) bool {
	return t.HasDomain(domain) && t.HasPath(req.URL.Path) && t.HasHeader(req.Header) && t.HasQuery(req.URL.Query()) && t.HasCookie(req)
}

func (t *reverseProxyTarget) HasDomain(domain string) bool {
	if t.Domain == "" {
		return true
	} else if strings.HasPrefix(t.Domain, ".") {
		return strings.HasSuffix(domain, t.Domain)
	}
	return domain == t.Domain
}

func (t *reverseProxyTarget) HasPath(path string) bool {
	if t.Path == "" {
		return true
	}
	return strings.HasPrefix(path, t.Path)
}

func (t *reverseProxyTarget) HasHeader(header http.Header) bool {
loop:
	for _, h := range t.Header {
		if values := header.Values(h.Name); len(values) > 0 {
			if h.Value == "" {
				continue
			} else {
				for _, val := range values {
					if h.Contains {
						if h.CaseInsensitive {
							if strings.Contains(strings.ToLower(val), strings.ToLower(h.Value)) {
								continue loop
							}
						} else {
							if strings.Contains(val, h.Value) {
								continue loop
							}
						}
					} else if h.CaseInsensitive {
						if strings.EqualFold(val, h.Value) {
							continue loop
						}
					} else {
						if val == h.Value {
							continue loop
						}
					}
				}
			}
		}
		return false
	}
	return true
}

func (t *reverseProxyTarget) HasQuery(query url.Values) bool {
loop:
	for _, q := range t.Query {
		if values, ok := query[q.Name]; ok {
			if q.Value == "" {
				continue
			} else {
				for _, val := range values {
					if q.Contains {
						if q.CaseInsensitive {
							if strings.Contains(strings.ToLower(val), strings.ToLower(q.Value)) {
								continue loop
							}
						} else {
							if strings.Contains(val, q.Value) {
								continue loop
							}
						}
					} else if q.CaseInsensitive {
						if strings.EqualFold(val, q.Value) {
							continue loop
						}
					} else {
						if val == q.Value {
							continue loop
						}
					}
				}
			}
		}
		return false
	}
	return true
}

func (t *reverseProxyTarget) HasCookie(req *http.Request) bool {
	for _, c := range t.Cookie {
		if cookie, err := req.Cookie(c.Name); err == nil {
			if c.Value == "" {
				continue
			} else {
				if c.Contains {
					if c.CaseInsensitive {
						if strings.Contains(strings.ToLower(cookie.Value), strings.ToLower(c.Value)) {
							continue
						}
					} else {
						if strings.Contains(cookie.Value, c.Value) {
							continue
						}
					}
				} else if c.CaseInsensitive {
					if strings.EqualFold(cookie.Value, c.Value) {
						continue
					}
				} else {
					if cookie.Value == c.Value {
						continue
					}
				}
			}
		}
		return false
	}
	return true
}
