package main

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"path"
	"syscall"
	"time"

	"github.com/pires/go-proxyproto"
	"gitlab.com/HiSakDev/go-certificate-reloader"
)

type reverseProxyServer struct {
	http.Server
	ctx      context.Context
	keyPair  *certreload.KeyPair
	caPool   *certreload.CAPool
	finalize func()
}

func newReverseProxyServer() (srv reverseProxyServer) {
	proxy := &httputil.ReverseProxy{}
	var ServerConfigureFunc func(*http.Server) error
	var TLSConfigureFunc func(*http.Server, []byte) (*certreload.KeyPair, *certreload.CAPool, error)
	var VerifyConnectionFunc func(tls.ConnectionState) error
	if config.Plugin.File != "" {
		if err := config.Plugin.Open(); err != nil {
			log.Fatal("http: plugin error: ", err)
		} else {
			if configure, err := config.Plugin.Lookup("Configure"); err == nil {
				if f, ok := configure.(func(string) error); ok {
					log.Print("http: plugin: call Configure")
					if err := f(config.Plugin.Config); err != nil {
						log.Fatal("http: plugin error: ", err)
					}
				}
			}
			if configure, err := config.Plugin.Lookup("ServerConfigure"); err == nil {
				if f, ok := configure.(func(*http.Server) error); ok {
					ServerConfigureFunc = f
				}
			}
			if configure, err := config.Plugin.Lookup("TLSConfigure"); err == nil {
				if f, ok := configure.(func(*http.Server, []byte) (*certreload.KeyPair, *certreload.CAPool, error)); ok {
					TLSConfigureFunc = f
				}
			}
			if verifyConn, err := config.Plugin.Lookup("VerifyConnection"); err == nil {
				if f, ok := verifyConn.(func(tls.ConnectionState) error); ok {
					VerifyConnectionFunc = f
				}
			}
			if final, err := config.Plugin.Lookup("Finalize"); err == nil {
				if f, ok := final.(func()); ok {
					log.Print("http: plugin: load Finalize")
					srv.finalize = f
				}
			}
			if errorLog, err := config.Plugin.Lookup("ErrorLog"); err == nil {
				log.Print("http: plugin: load ReverseProxy.ErrorLog")
				proxy.ErrorLog = errorLog.(*log.Logger)
			}
			if flushInterval, err := config.Plugin.Lookup("FlushInterval"); err == nil {
				log.Print("http: plugin: load ReverseProxy.FlushInterval")
				proxy.FlushInterval = *flushInterval.(*time.Duration)
			}
			if bufferPool, err := config.Plugin.Lookup("BufferPool"); err == nil {
				log.Print("http: plugin: load ReverseProxy.BufferPool")
				proxy.BufferPool = *bufferPool.(*httputil.BufferPool)
			}
			if transport, err := config.Plugin.Lookup("Transport"); err == nil {
				log.Print("http: plugin: load ReverseProxy.Transport")
				proxy.Transport = *transport.(*http.RoundTripper)
			}
			if director, err := config.Plugin.Lookup("Director"); err == nil {
				log.Print("http: plugin: load ReverseProxy.Director")
				proxy.Director = director.(func(*http.Request))
			}
			if modifyResponse, err := config.Plugin.Lookup("ModifyResponse"); err == nil {
				log.Print("http: plugin: load ReverseProxy.ModifyResponse")
				proxy.ModifyResponse = modifyResponse.(func(*http.Response) error)
			}
			if errorHandler, err := config.Plugin.Lookup("ErrorHandler"); err == nil {
				log.Print("http: plugin: load ReverseProxy.ErrorHandler")
				proxy.ErrorHandler = errorHandler.(func(http.ResponseWriter, *http.Request, error))
			}
			if handler, err := config.Plugin.Lookup("Handler"); err == nil {
				log.Print("http: plugin: load Server.Handler")
				srv.Handler = handler.(func(http.Handler) http.Handler)(proxy)
			}
		}
	} else {
		proxy.Director = func(req *http.Request) {
			domain := req.Host
			if host, _, err := net.SplitHostPort(domain); err == nil {
				domain = host
			}
			for _, target := range config.Target {
				if target.Match(domain, req) {
					req.URL.Scheme = target.URL.Scheme
					req.URL.Host = target.URL.Host
					req.Host = target.URL.Host
					req.URL.Path = path.Join(target.URL.Path, target.RewritePath(req.URL.Path))
					if target.URL.RawQuery == "" || req.URL.RawQuery == "" {
						req.URL.RawQuery = target.URL.RawQuery + req.URL.RawQuery
					} else {
						req.URL.RawQuery = target.URL.RawQuery + "&" + req.URL.RawQuery
					}
					for _, header := range target.RequestHeader {
						if header.Value == "" {
							req.Header.Del(header.Name)
						} else {
							req.Header.Add(header.Name, header.Value)
						}
					}
					forward := target.URL.String()
					if target.Name != "" {
						forward = "<" + target.Name + ">"
					}
					ctx := req.Context().Value("ctx").(*context.Context)
					*ctx = context.WithValue(*ctx, "forward", forward)
					*ctx = context.WithValue(*ctx, "target", target)
					break
				}
			}
		}
		proxy.ModifyResponse = func(res *http.Response) error {
			ctx := res.Request.Context().Value("ctx").(*context.Context)
			if target, ok := (*ctx).Value("target").(reverseProxyTarget); ok {
				for _, header := range target.ResponseHeader {
					if header.Value == "" {
						res.Header.Del(header.Name)
					} else {
						res.Header.Add(header.Name, header.Value)
					}
				}
			}
			*ctx = context.WithValue(*ctx, "response", res)
			return nil
		}
		proxy.ErrorHandler = func(w http.ResponseWriter, r *http.Request, err error) {
			log.Print("http: proxy error: ", err)
			ctx := r.Context().Value("ctx").(*context.Context)
			errorCode := 502
			errorConfig := config.Error
			if target, ok := (*ctx).Value("target").(reverseProxyTarget); ok {
				if target.Error != nil {
					errorConfig = *target.Error
				}
			}
			if errorConfig.StatusCode != 0 {
				errorCode = errorConfig.StatusCode
			}
			if errorConfig.File != "" {
				if data, err := ioutil.ReadFile(errorConfig.File); err == nil {
					for _, header := range errorConfig.ResponseHeader {
						if header.Value == "" {
							w.Header().Del(header.Name)
						} else {
							w.Header().Add(header.Name, header.Value)
						}
					}
					for _, cookie := range errorConfig.ResponseCookie {
						http.SetCookie(w, cookie)
					}
					if errorConfig.ContentType != "" {
						w.Header().Set("Content-Type", errorConfig.ContentType)
					}
					w.WriteHeader(errorCode)
					if _, err = w.Write(data); err != nil {
						log.Print("http: error handle: ", err)
					}
					return
				} else {
					log.Print("http: error handle: ", err)
				}
			}
			w.WriteHeader(errorCode)
		}
	}
	if srv.Handler == nil {
		srv.Handler = accessLogHandler(proxy)
	}
	if ServerConfigureFunc != nil {
		if err := ServerConfigureFunc(&srv.Server); err != nil {
			log.Fatal("http: plugin error: ", err)
		}
	}
	srv.ctx = context.Background()

	if TLSConfigureFunc != nil {
		if b, err := json.Marshal(config.TLS); err != nil {
			log.Fatal("http: tls encode error: ", err)
		} else {
			log.Print("http: plugin: call TLSConfigure")
			if srv.keyPair, srv.caPool, err = TLSConfigureFunc(&srv.Server, b); err != nil {
				log.Fatal("http: plugin error: ", err)
			}
		}
	} else if config.TLS.Cert != "" && config.TLS.Key != "" {
		if keyPair, err := certreload.New(config.TLS.Cert, config.TLS.Key); err != nil {
			log.Fatal("http: keyPair error: ", err)
		} else {
			keyPair.Logger = log.New(os.Stderr, "", log.LstdFlags)
			srv.TLSConfig = &tls.Config{
				GetCertificate:           keyPair.GetCertificate,
				MinVersion:               config.TLS.MinVersion,
				MaxVersion:               config.TLS.MaxVersion,
				PreferServerCipherSuites: config.TLS.PreferServerCiphers,
				CipherSuites:             config.TLS.Ciphers,
				ClientAuth:               config.TLS.ClientAuth,
			}
			if config.TLS.ClientCA != "" {
				if caPool, err := certreload.NewPool(config.TLS.ClientCA); err != nil {
					log.Fatal("http: caPool error: ", err)
				} else {
					caPool.Logger = log.New(os.Stderr, "", log.LstdFlags)
					srv.TLSConfig.ClientCAs = caPool.CAs
					srv.caPool = &caPool
				}
			}
			if VerifyConnectionFunc != nil {
				log.Print("http: plugin: load TLS.VerifyConnection")
				srv.TLSConfig.VerifyConnection = VerifyConnectionFunc
			}
			if !config.TLS.Http2 {
				srv.TLSNextProto = make(map[string]func(*http.Server, *tls.Conn, http.Handler))
			}
			srv.keyPair = &keyPair
		}
	}

	return
}

func (srv *reverseProxyServer) run() {
	listener, err := net.Listen("tcp", config.Listen)
	if err != nil {
		log.Fatal("http: listen error: ", err)
	}

	proxyListener := &proxyproto.Listener{Listener: listener}
	log.Printf("http: proxy server start listen=%v tls=%v", config.Listen, srv.TLSConfig != nil)
	defer func() {
		if srv.finalize != nil {
			srv.finalize()
		}
		_ = proxyListener.Close()
	}()
	if srv.TLSConfig != nil {
		if srv.keyPair != nil {
			srv.keyPair.Watch()
			defer srv.keyPair.Stop()
		}
		if srv.caPool != nil {
			srv.caPool.Watch()
			defer srv.caPool.Stop()
		}
		if err = srv.ServeTLS(proxyListener, "", ""); err != nil && err != http.ErrServerClosed {
			log.Print("http: server error: ", err)
		}
	} else {
		if err = srv.Serve(proxyListener); err != nil && err != http.ErrServerClosed {
			log.Print("http: server error: ", err)
		}
	}
}

func (srv *reverseProxyServer) stop() error {
	log.Printf("http: proxy server shutdown")
	ctx, cancel := context.WithTimeout(srv.ctx, config.ShutdownTimeout.Duration)
	defer cancel()
	return srv.Shutdown(ctx)
}

func main() {
	if err := settingParse(); err != nil {
		log.Fatal("env: settingParse error: ", err)
	}
	control := make(chan os.Signal, 1)
	signal.Notify(control, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	for {
		if err := configLoad(); err != nil {
			log.Fatal("toml: configLoad error: ", err)
		}
		server := newReverseProxyServer()
		go server.run()
		sig := <-control
		if err := server.stop(); err != nil {
			log.Print("http: shutdown error: ", err)
		}
		if sig == os.Interrupt || sig == syscall.SIGTERM {
			break
		}
	}
}
