module gitlab.com/HiSakDev/go-rproxy

go 1.20

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pires/go-proxyproto v0.7.0
	gitlab.com/HiSakDev/go-certificate-reloader v0.2.0
)
