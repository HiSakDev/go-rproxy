package main

import (
	"plugin"
)

type reverseProxyPlugin struct {
	*plugin.Plugin
	File   string
	Config string
}

func (p *reverseProxyPlugin) Open() (err error) {
	p.Plugin, err = plugin.Open(p.File)
	return
}
